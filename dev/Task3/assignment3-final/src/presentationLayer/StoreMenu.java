package presentationLayer;

import presentationLayer.Sup_Inv_PresentationLayer.MenuController;

public class StoreMenu {
    public void run() throws Exception {
        System.out.println("Welcome to Store Keeper Menu:");
        MenuController.getInstance().StoreKeeperMenu();
    }
}
