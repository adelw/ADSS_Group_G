package presentationLayer;

import presentationLayer.Sup_Inv_PresentationLayer.MenuController;

public class supplierMenu {

    public void run(String type) throws Exception {
        if (type.equals("JustOrderSupplier") || type.equals("CollectingSupplier")){
            MenuController.getInstance().SupplierType3or2Menu();
        }else{
            MenuController.getInstance().SupplierType1Menu();
        }
    }
}
