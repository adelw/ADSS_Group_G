package UnitTests;
import businessLayer.*;
import businessLayer.Sup_Inv.*;
import businessLayer.deliveryPackage.Controllers.DeliveryController;
import businessLayer.deliveryPackage.Controllers.DriverController;
import businessLayer.deliveryPackage.Controllers.TruckController;
import businessLayer.employeePackage.EmployeeController;
import businessLayer.employeePackage.RegularEmployee;
import businessLayer.shiftPackage.ShiftController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

//

public class AllSystemTests {
    DeliveryController deliveryController;
    DriverController driverController;
    TruckController truckController;
    EmployeeController employeeController;
    RegularEmployee regularEmployee;
    ShiftController shiftController;
    SupplierController supplierController;
    CategoryController categoryController;
    OrderController orderController;
    ContractController contractController;
    InventoryController inventoryController;
    ProductController productController;
    ReportController reportController;
//
//
    @Before
    public void setUp() throws Exception {
        deliveryController = new DeliveryController();
        driverController = new DriverController();
        truckController = new TruckController();
        employeeController = new EmployeeController();
        shiftController = shiftController = new ShiftController();
        supplierController = SupplierController.getInstance();
        categoryController = CategoryController.getInstance();
        orderController = OrderController.getInstance();
        contractController = new ContractController();
        inventoryController = InventoryController.getInstance();
        productController =ProductController.getInstance();
        reportController = ReportController.getInstance();
    }
//
//    @After
//    public void Remove(){
//        inventoryController.getDBHandler().cleanDB();
//    }
//
//    /*@Test
//    public void addSupplier() {
//        supplierController.AddSupplierTest("moslem","jatt","0586868437",1,"123, boalem",123);
//        assertEquals(2,supplierController.getSuppliers_id().size());
//    }
//
//    @Test
//    public void addSupplierFailName() {
//        try {
//            supplierController.AddSupplier("", "akko", "055555555"
//                    , 1, "10256, doar", 520);
//        } catch (Exception e) {
//            assertEquals(e.getMessage(), "name cannot be empty");
//        }
//    }
//
//    @Test
//    public void addSupplierFailAddress() {
//        try {
//            supplierController.AddSupplier("ana", "", "055555555"
//                    , 1, "10256, doar", 520);
//        } catch (Exception e) {
//            assertEquals(e.getMessage(), "address cannot be empty");
//        }
//    }
//
//    @Test
//    public void makeContractFail() {
//        try{
//            Map<Integer, java.util.Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Double> price = new HashMap<>();
//            contractController.makeContract(-1,123,true,QuantityDiscountPerProduct,price);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"the supplier id is not legal");
//        }
//    }
//    @Test
//    public void deleteSupplierFail() {
//        try{
//            supplierController.DeleteSupplier(10);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"the supplier is not exist in the system");
//        }
//    }
//    @Test
//    public void deleteSupplier() {
//        try{
//            supplierController.DeleteSupplier(1);
//            assertEquals(0,supplierController.getSuppliers_id().size());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void AddCategory(){
//        try{
//            categoryController.insertCategory(new Category(1,"drinks",null));
//            assertEquals(3,categoryController.getCategories().size());
//        }catch (Exception e){
//
//        }
//    }
//
//    @Test
//    public void AddCategoryFail(){
//        try{
//            categoryController.insertCategory(null);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"category can't be null!");
//        }
//    }
//
//    @Test
//    public void AddSubCategory(){
//        try{
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            Category s2 = new Category(2,"cold-drinks",s1);
//            categoryController.insertCategory(s2);
//            assertEquals(6,categoryController.getCategories().size());
//            assertEquals(s2.getSupCategory().getId(),1);
//        }catch (Exception e){
//
//        }
//    }
//
//    @Test
//    public void AddGeneralProductFail(){
//        try{
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",null);
//            productController.addGeneralProduct(100,"milk",444,"tnova",100,null);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"the product is exist in the system");
//        }
//    }
//
//    @Test
//    public void AddGeneralProductSuccess(){
//        try{
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",s1);
//            productController.addGeneralProduct(100,"milk",444,"tnova",100,s1);
//            assertEquals(1,productController.getProducts().size());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void AddSpecificProductFail(){
//        try{
//            inventoryController.addSpecificProducts(-1,"25/12/2022");
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"Code not found");
//        }
//    }
//
//    @Test
//    public void AddSpecificProductSuccess(){
//        try{
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",s1);
//            productController.addGeneralProduct(100,"milk",444,"tnova",100,s1);
//            int size = productController.getProducts().size();
//            inventoryController.addSpecificProducts(444,"25/12/2022");
//            assertEquals(productController.getProducts().size(),size+1);
//        }catch (Exception e){
//        }
//    }
//*/
//
//    /////////////////////////FROM HERE COMPINED TESTS/////////////////////////////////////////////
//    @Test
//    public void MakeContractAndUpdateCostPriceFail(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",s1);
//            productController.addGeneralProduct(100,"milk",444,"tnova",100,s1);
//            int size = productController.getProducts().size();
//            inventoryController.addSpecificProducts(444,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(120,q_d);
//            price.put(120,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"can't make a contract with unexisted product code");
//        }
//    }
//
//    @Test
//    public void MakeContractAndUpdateCostPriceSuccess(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",s1);
//            productController.addGeneralProduct(444,"milk",444,"tnova",100,s1);
//            int size = productController.getProducts().size();
//            inventoryController.addSpecificProducts(444,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(444,q_d);
//            price.put(444,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//           // assertEquals(50.0,productController.getProducts().get(444).getCostPrice());
//            assertEquals(50.0,productController.getProducts().get(444).getCostPrice(),0.0);
//            ///  inventoryController.getDBHandler().cleanDB();
//        }catch (Exception e){
//         //   inventoryController.getDBHandler().cleanDB();
//        }
//    }
//
//    @Test
//    public void MakeContractwithMultibleProducts(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            productController.addGeneralProduct(433,"milk",15.5,"tnova",100,s1);
//            productController.addGeneralProduct(555,"shoko",10.9,"tnova",50,s1);
//            inventoryController.addSpecificProducts(433,"25/12/2022");
//            inventoryController.addSpecificProducts(555,"13/09/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(444,q_d);
//            price.put(433,15.5);
//            Map<Integer,Integer> q_d2 = new HashMap<>();
//            q_d2.put(50,5);
//            q_d2.put(150,15);
//            QuantityDiscountPerProduct.put(555,q_d2);
//            price.put(555,10.9);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            assertEquals(2,contractController.getSupplierid_contract().get(1).size());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void MakeContractwithMultibleProductsSomeNotExist(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            productController.addGeneralProduct(544,"shoko",10.9,"tnova",50,s1);
//            inventoryController.addSpecificProducts(422,"25/12/2022");
//            inventoryController.addSpecificProducts(544,"13/09/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(444,q_d);
//            price.put(422,15.5);
//            Map<Integer,Integer> q_d2 = new HashMap<>();
//            q_d2.put(50,5);
//            q_d2.put(150,15);
//            QuantityDiscountPerProduct.put(555,q_d2);
//            price.put(544,10.9);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"Code not found");
//        }
//    }
//
//    @Test
//    public void CheckUpdateAllProductsPrices(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            productController.addGeneralProduct(411,"milk",15.5,"tnova",100,s1);
//            productController.addGeneralProduct(566,"shoko",10.9,"tnova",50,s1);
//            inventoryController.addSpecificProducts(411,"25/12/2022");
//            inventoryController.addSpecificProducts(566,"13/09/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(444,q_d);
//            price.put(411,15.5);
//            Map<Integer,Integer> q_d2 = new HashMap<>();
//            q_d2.put(50,5);
//            q_d2.put(150,15);
//            QuantityDiscountPerProduct.put(555,q_d2);
//            price.put(566,10.9);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            assertEquals(15.5,productController.getProducts().get(411).getCostPrice(),0.0);
//            assertEquals(10.9,productController.getProducts().get(566).getCostPrice(),0.0);
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void AddOrderWithSpecificProductFails(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",444,50,0,"tnova",s1);
//            productController.addGeneralProduct(100,"milk",444,"tnova",100,s1);
//            int size = productController.getProducts().size();
//            inventoryController.addSpecificProducts(900,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(900,q_d);
//            price.put(900,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            orderController.addOrder(2022,5,22,100,500);
//        }catch (Exception e){
//            assertEquals(e.getMessage(),"the product is exist in the system");
//        }
//    }
//
//    @Test
//    public void AddOrderWithSpecificProductSuccessWithLowesPrice(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            supplierController.AddSupplier("moslem2", "jatt2", "055953621", 2, "223, boalem", 333);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",400,50,0,"tnova",s1);
//            productController.addGeneralProduct(400,"milk",444,"tnova",100,s1);
//            inventoryController.addSpecificProducts(400,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(400,q_d);
//            price.put(400,50.0);
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct2 = new HashMap<>();
//            Map<Integer,Integer> q_d2 = new HashMap<>();
//            q_d2.put(100,40);
//            Map<Integer,Double> price2 = new HashMap<>();
//            QuantityDiscountPerProduct2.put(400,q_d2);
//            price2.put(400,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            contractController.makeContract(2,553,false,QuantityDiscountPerProduct2,price2);
//            Order o =orderController.addOrder(2022,5,22,400,500);
//            assertEquals(2,o.getSupplier_id());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void AddOrderWithSpecificProductSuccessWithexceeding_min_quantity(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",333,50,0,"tnova",s1);
//            productController.addGeneralProduct(333,"milk",444,"tnova",50,s1);
//            inventoryController.addSpecificProducts(333,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(333,q_d);
//            price.put(333,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            Order o =orderController.addOrder(2022,5,22,333,20);
//            assertEquals(70,o.getQuantity());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void AddOrderWithSpecificProductSuccessWithAmountExceedsMinQuantityAlready(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",999,50,0,"tnova",s1);
//            productController.addGeneralProduct(999,"milk",444,"tnova",50,s1);
//            inventoryController.addSpecificProducts(999,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(999,q_d);
//            price.put(999,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            Order o =orderController.addOrder(2022,5,22,999,70);
//            assertEquals(70,o.getQuantity());
//        }catch (Exception e){
//        }
//    }
//
//    @Test
//    public void CheckProductQuantityIncreasedAfterAddingOrder(){
//        try {
//            supplierController.AddSupplier("moslem", "jatt", "05595321", 1, "123, boalem", 123);
//            Category s1 = new Category(1,"drinks",null);
//            categoryController.insertCategory(s1);
//            GeneralProduct gp1 = new GeneralProduct(100,"milk",888,50,0,"tnova",s1);
//            productController.addGeneralProduct(888,"milk",444,"tnova",50,s1);
//            inventoryController.addSpecificProducts(888,"25/12/2022");
//            Map<Integer, Map<Integer,Integer>> QuantityDiscountPerProduct = new HashMap<>();
//            Map<Integer,Integer> q_d = new HashMap<>();
//            q_d.put(100,10);
//            Map<Integer,Double> price = new HashMap<>();
//            QuantityDiscountPerProduct.put(888,q_d);
//            price.put(888,50.0);
//            contractController.makeContract(1,123,true,QuantityDiscountPerProduct,price);
//            Order o =orderController.addOrder(2022,5,22,888,70);
//            GeneralProduct p = productController.getGeneralProductByCode(888);
//            assertEquals(70,p.getQuantity());
//        }catch (Exception e){
//        }
//    }
//
//
//}

}
