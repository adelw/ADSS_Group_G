package businessLayer.deliveryPackage;

import businessLayer.deliveryPackage.Controllers.DeliveryController;
import businessLayer.deliveryPackage.Controllers.DriverController;
import businessLayer.deliveryPackage.Controllers.TruckController;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Facade {
    private DeliveryController deliveryController;
    private DriverController driverController;
    private TruckController truckController;

    public Facade() throws Exception {
        deliveryController = new DeliveryController();
        driverController = new DriverController();
        truckController = new TruckController();
    }
    // ------------------ Delivery ------------------
//    public Response<Delivery> getDelivery(Date date, String launchTime, String plateNumber){
//        try{
//            return new Response<>(deliveryController.getDelivery(date, launchTime, plateNumber));
//        } catch (Exception e){
//            return new Response<>(e.getMessage());
//        }
//    }

    public Response<Delivery> getDelivery(int deliveryId){
        try{
            return new Response<>(deliveryController.getDelivery(deliveryId));
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response<List<Delivery>> getAllDeliveries(){
        try{
            return new Response<>(deliveryController.getAllDeliveries());
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response addDelivery(Date date, String launchTime, Location source, Map<Location, LinkedList<Item>> items){
        try {
            double itemsWeight = 0;
            for(LinkedList<Item> list : items.values()){
                itemsWeight += Item.calculateWeight(list);
            }
            String shift = convertTimeToShift(launchTime);
            //System.out.println("0");
            deliveryController.checkIfShiftExist(shift, date);
            //System.out.println("1");
            deliveryController.checkStoreKeeperInShift(shift, date);
            //System.out.println("2");
            Truck truck = truckController.getAvailableTruck(date, shift, itemsWeight);
            //System.out.println("3");
            double totalWeight = itemsWeight + truck.getNetWeight();
            Driver driver = driverController.getAvailableDriver(date, shift, totalWeight);
           // System.out.println("4");
            Delivery delivery = deliveryController.addDelivery(date, launchTime, truck.getPlateNumber(), driver.getId(), source, items, totalWeight);
            //System.out.println("5");
            deliveryController.addTruckScheduler(truck.getPlateNumber(), date, shift);
            //System.out.println("6");
            return new Response();
        } catch (Exception e){
            return new Response(e.getMessage());
        }
    }

//    public Response<Delivery> cancelDelivery(Date date, String launchTime, String truckPlateNumber){
//        try {
//            Delivery delivery = deliveryController.cancelDelivery(date, launchTime, truckPlateNumber);
//            return new Response<>(delivery);
//        } catch (Exception e){
//            return new Response<>(e.getMessage());
//        }
//    }

    public Response<Delivery> cancelDelivery(int deliveryId){
        try {
            Delivery delivery = deliveryController.cancelDelivery(deliveryId);
            return new Response<>(delivery);
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response<Map<Delivery, Document>> getDocument(int documentId){
        try {
            return new Response<>(deliveryController.getDocument(documentId));
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response<List<Document>> getDeliveryDocuments(int deliveryId){
        try {
            return new Response<>(deliveryController.getDeliveryDocuments(deliveryId));
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    private String convertTimeToShift(String time){
        String[] split = time.split(":");
        try{
            if(Integer.parseInt(split[0]) < 17)
                return "morning";
            else
                return "evening";
        } catch (Exception e){

        }
        return "";
    }

    public Item newItem(String name, double amount, double weight) throws Exception{
        if(name.length() == 0 || amount <= 0 || weight <= 0)
            throw new Exception("Invalid Item's details");
        return new Item(name, amount, weight);
    }

    public Response<Location> newLocation(String address, String phoneNumber, String contactName) throws Exception{
        try {
            if(address == null || address.length() == 0 || phoneNumber == null || phoneNumber.length() != 10 || contactName == null || contactName.length() == 0)
                throw new Exception("Invalid input");
            try {
                new BigInteger(phoneNumber); // check if this is a number (not contains characters)
            }catch (Exception e){
                throw new Exception("Invalid phone number");
            }
            return new Response<>(new Location(address, phoneNumber, contactName));
        }catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    // ------------------ Truck ------------------
    public Response<Truck> getTruck(String plateNumber){
        try {
            return new Response<>(truckController.getTruck(plateNumber));
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response<List<Truck>> getAllTrucks(){
        try{
            return new Response<>(truckController.getAllTrucks());
        }catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

    public Response addTruck(String plateNumber, String model, double netWeight, double maxWeight){
        try {
            truckController.addTruck(plateNumber, model, netWeight, maxWeight);
            return new Response();
        }catch (Exception e){
            return new Response(e.getMessage());
        }
    }

    public Response<Truck> deleteTruck(String plateNumber){
        try {
            Truck truck = truckController.deleteTruck(plateNumber);
            return new Response<>(truck);
        } catch (Exception e){
            return new Response<>(e.getMessage());
        }
    }

}
