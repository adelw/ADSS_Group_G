package businessLayer.deliveryPackage;

public class Driver {
    private String id;
    private String name;
    private double goodsWeightLimit;
    private boolean available;

    public Driver(String id, String name, double goodsWeightLimit){
        this.id = id;
        this.name = name;
        this.goodsWeightLimit = goodsWeightLimit;
        available = true;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public double getGoodsWeightLimit(){
        return goodsWeightLimit;
    }

    public void setGoodsWeightLimit(double weightLimit){
        this.goodsWeightLimit = weightLimit;
    }

    public boolean isAvailable(){
        return available;
    }

    public void setAvailable(boolean available){
        this.available = available;
    }
}
