package businessLayer.deliveryPackage;

import java.util.List;

public class Item {
    private String name;
    private double amount;
    private double itemWeight;
    private double totalWeight;

    public Item(String name, double amount, double itemWeight){
        this.name = name;
        this.amount = amount;
        this.itemWeight = itemWeight;
        totalWeight = amount * itemWeight;
    }

    public String getName(){
        return name;
    }

    public double getAmount(){
        return amount;
    }

    public double getItemWeight(){
        return itemWeight;
    }

    public double getTotalWeight(){
        return amount * itemWeight;
    }

    public static double calculateWeight(List<Item> items){
        double weight = 0;
        for(Item item : items){
            weight += item.getTotalWeight();
        }
        return weight;
    }
}
