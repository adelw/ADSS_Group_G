package presentationLayer;
import businessLayer.deliveryPackage.Facade;
import businessLayer.deliveryPackage.Item;
import businessLayer.deliveryPackage.Location;
import presentationLayer.DeliveryController;
import presentationLayer.TruckController;

import java.text.SimpleDateFormat;
import java.util.*;

public class DeliveryMenu {

    public void run() throws Exception {
        Scanner scanner = new Scanner(System.in);
        Facade facade = new Facade();
//        try{
//            facade = new Facade();
//        }catch (Exception e){

        //}
        DeliveryController deliveryController = new DeliveryController(facade);
        TruckController truckController = new TruckController(facade);
        String  input;
        boolean finish = false;
        while (!finish){
            Menu();
            input = scanner.next();
            if(validInput(input)){
                switch (Integer.parseInt(input)){
                    case 1:
                        deliveryController.addDelivery();
                        break;
                    case 2:
                        deliveryController.cancelDelivery();
                        break;
                    case 3:
                        deliveryController.getDelivery();
                        break;
                    case 4:
                        deliveryController.viewAllDeliveries();
                        break;
                    case 5:
                        deliveryController.viewDocument();
                        break;
                    case 6:
                        truckController.addTruck();
                        break;
                    case 7:
                        truckController.deleteTruck();
                        break;
                    case 8:
                        truckController.getTruck();
                        break;
                    case 9:
                        truckController.viewAllTrucks();
                        break;
                    case 10:
                        finish = true;
                        break;
                }
            }
            else
                System.out.println("Select valid number");
            System.out.println();
        }
    }

    private static boolean validInput(String input){
        try {
            int x = Integer.parseInt(input);
            if(x > 0 && x <= 10)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    private static void Menu(){
        System.out.println("Welcome, Choose one of the following options:");
        System.out.println("1. Add Delivery");
        System.out.println("2. Cancel Delivery");
        System.out.println("3. View Delivery");
        System.out.println("4. View all deliveries");
        System.out.println("5. View document");
        System.out.println("6. Add truck");
        System.out.println("7. Delete truck");
        System.out.println("8. View truck");
        System.out.println("9. View all trucks");
        System.out.println("10. Exit");
    }
}