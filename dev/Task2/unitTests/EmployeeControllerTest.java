package unitTests;
import businessLayer.employeePackage.EmployeeController;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.IllegalFormatException;

import static org.junit.jupiter.api.Assertions.*;
public class EmployeeControllerTest {
    private EmployeeController employeeController=new EmployeeController();

    public EmployeeControllerTest() throws Exception {

    }


    @Test
    void loginWithExistEmployee() throws Exception {
        try {
            employeeController.login("123456789");
            fail();
        }catch (Exception e){

        }
    }

    @Test
    void loginWithNotValidEmployeeID() {
        try {
            employeeController.login("2");
            fail();
        }catch (Exception e){


        }
    }

    @Test
    void addConstraintWithNotLegalday() throws Exception {
        int day= 7;
        String shift="morning";
        String reason="medicalIssue";
        employeeController.setLoggedIn("123456789");
        try {
            employeeController.addConstraint(day,shift,reason);
            fail();
        }catch (Exception e){

        }
    }

    @Test
    void addEmployeeWithExistID() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date = format.parse("02/05/2021");
        try {
            employeeController.createEmployee("saeed","206469017",654321,726,"hapoalem",9000,date,"Driver","Full Time",false);
            fail();
        }catch (Exception e){

        }

    }

    @Test
    void deleteConstraintNotExist() {
        employeeController.setLoggedIn("123456789");
        try {
            employeeController.deleteConstraint(1,"morning");
            fail();
        } catch (Exception e) {

        }
    }





}
