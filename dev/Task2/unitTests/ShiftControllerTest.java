package unitTests;
import businessLayer.shiftPackage.Shift;
import businessLayer.shiftPackage.ShiftController;
import dataAccessLayer.DalController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;


import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
//
class ShiftControllerTest {

    ShiftController shifts;





    @BeforeEach
    void setUp() throws Exception {

        shifts=new ShiftController();

    }

    @Test
    void createShift() throws Exception {
        Date today = null;
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        today = calendar.getTime();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 2);
        today = calendar.getTime();
        if(today.toString().substring(0, 3).equals("Fri") || today.toString().substring(0, 3).equals("Sat")){
            calendar.add(Calendar.DATE, 2);
            today = calendar.getTime();
        }
        shifts.createShift(4,today,"evening");
        Assert.assertEquals(1,shifts.getShifts().size());
    }

    @Test
    void updateShiftWithAssignEmployee() throws Exception {
        Date today = null;
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        today = calendar.getTime();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 2);
        today = calendar.getTime();
        if(today.toString().substring(0, 3).equals("Fri") || today.toString().substring(0, 3).equals("Sat")){
            calendar.add(Calendar.DATE, 2);
            today = calendar.getTime();
        }

        ArrayList<Shift> shiftArrayList=new ArrayList<>();
        DalController.getInstance().assignEmployee(today,"morning","123456789");


        try {
            shifts.updateShift(today,"morning","cashier","123456789");
        }catch (Exception e){
            Assert.assertEquals("Employee Is Already Assigned To a Job In This Shift",e.getMessage());
        }

    }

    @Test
    void updateUnExistShift() throws ParseException {
        Date today = null;
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        today = calendar.getTime();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 2);
        today = calendar.getTime();
        if(today.toString().substring(0, 3).equals("Fri") || today.toString().substring(0, 3).equals("Sat")){
            calendar.add(Calendar.DATE, 2);
            today = calendar.getTime();
        }
        try{
            shifts.updateShift(today,"morning","cashier","123456789");

        }catch (Exception e){
            Assert.assertEquals("There are no shifts with this specific date",e.getMessage());
        }
    }


    @Test
    void deleteUnExistShift() throws ParseException {

        Date today = null;
        Calendar calendar;
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        today = calendar.getTime();
        calendar.setTime(today);
        calendar.add(Calendar.DATE, 2);
        today = calendar.getTime();
        if(today.toString().substring(0, 3).equals("Fri") || today.toString().substring(0, 3).equals("Sat")){
            calendar.add(Calendar.DATE, 2);
            today = calendar.getTime();
        }

        try {
            shifts.deleteShift(today,"evening");
        }catch (Exception e){
            Assert.assertEquals("There are no shifts with this specific date",e.getMessage());
        }

    }


}