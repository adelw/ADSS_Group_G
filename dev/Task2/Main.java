import dataAccessLayer.DalController;
import presentationLayer.DeliveryMenu;
import presentationLayer.Menu;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws Exception {
        boolean finish = false;
        while(!finish){
            System.out.println("****************************************");
            System.out.println("WELCOME      TO         SUPER        LEE");
            System.out.println("****************************************");
            System.out.println("1) EMPLOYEE MODULE");
            System.out.println("2) DELIVERY MODULE");
            System.out.println("3) EXIT");
            System.out.print("Please choose an option: ");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            if(validInput(choice)){
                switch(Integer.parseInt(choice)){
                    case 1:
                        Menu menu = Menu.getInstance();
                        menu.run();
                        break;
                    case 2:
                        DeliveryMenu deliveryMenu = new DeliveryMenu();
                        deliveryMenu.run();
                        break;
                    case 3:
                        System.out.println("**** See You Later :) ****");
                        finish = true;
                        break;
                }
            }
        }
    }

    private static boolean validInput(String input){
        try {
            int x = Integer.parseInt(input);
            if(x > 0 && x <= 3)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
